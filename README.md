# GMod Piano Player
## Version 1.0.1

WARNING, BY USING THIS APPLICATION YOU ACCEPT THE RISK OF IT POSSIBLY DAMAGING YOUR COMPUTER,

YOU HAVE BEEN WARNED. ( probably won't happen, but just to be safe )

### [Instructions]

#### [Running]
to run simply double click run.bat

#### [Changing song]

1) open song.txt, or copy example song.

2) the first line is a number representing the amount of time between each note.

3) the second line is a number representing times to repeat ( -1 for forever )

4) all lines after are considered the song.

#### [Song notation]
1) normal text ie. "abcd" will be played a b c d.

2) text surounded by [] ie "[ab]" will play all notes in between at the same time.

3) anything surounded by {} will play at double speed.

4) adding a #number#, number being the new tempo you want will change the tempo.

5) only characters used by the gmod playable piano will work.

#### [Other]
1) if you wish to dabble with the code, I used groovy, and I used gradle to compile it.

2) you can contact me at thethreedudes73@gmail.com

3) thanks for using my app.	
