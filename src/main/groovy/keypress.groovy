// KEYPRESSER //

import java.awt.Robot
import java.awt.event.KeyEvent
import java.awt.AWTKeyStroke

class keypress {

	static void main( String... args ) {

		def conts = []
		new File( 'song.txt' ).eachLine { line ->
			conts << line
		}

		String song = ""

		conts[2..conts.size()-1].each {
			song += it
		}

		def keyboard = new key_presser( KEY_SPEED: conts[0].toInteger(), REPEAT_TIMES: conts[1].toInteger(), SONG: song )
		
		Thread.sleep( 5000 )

		keyboard.playSong()
	}

}



class key_presser {

	int KEY_SPEED
	int REPEAT_TIMES

	String SONG

	def shiftKeys = ['!','@','$','%','^','*','(']

	def board = new Robot()

	def press( key, delay ) {
		def keys = []
		key.each {
			if ( shiftKeys.contains( it ) || !it.equals( it.toLowerCase() ) ) {
				board.keyPress( KeyEvent.VK_SHIFT )
			}
		}
		board.delay( 30 )
		key.each {
			//CHECKS IF KEY NEEDS FIXED//

			switch( it ) {
				case '!':
					it = '1'
					break
				case '@':
					it = '2'
					break
				case '$':
					it = '4'
					break
				case '%':
					it = '5'
					break
				case '^':
					it = '6'
					break
				case '*':
					it = '8'
					break
				case '(':
					it = '9'
					break
			}

			def code = AWTKeyStroke.getAWTKeyStroke( it.toUpperCase() ).getKeyCode()

			board.keyPress( code )
			keys << code

		}

		board.delay( delay + 30 )
		board.keyRelease( KeyEvent.VK_SHIFT )

		keys.each {
			board.keyRelease( it )
		}
		board.delay( 30 )
	}

	def playSong() {
		int times = REPEAT_TIMES
		while ( times > 0 || times <= -1 ) {
			int speed = 2
			int i
			for ( i = 0; i < SONG.size(); i++ ) {
				String pos = SONG.substring( i, i + 1 )
				if ( pos == " " ) {
					int space = 0
					for ( number in i..SONG.size() - 1 ) {
						String pos2 = SONG.substring( number, number + 1 )
						if ( pos2 == " " ) {
							space += 1
							if ( number == SONG.size() - 1 ) {
								board.delay( ( KEY_SPEED * speed + 90 ) * space )
								i = number
								break
							}
						} else {
							board.delay( ( KEY_SPEED * speed + 90 ) * space )
							i = number - 1
							break
						}
					}
				} else if ( pos == "{" ) {
					speed = 1
				} else if ( pos == "}" ) {
					speed = 2
				} else if ( pos == "[" ) {
					String pressKey = ""
					for ( number in i..SONG.size() - 1 ) {
						String pos2 = SONG.substring( number, number + 1 )
						if ( pos2 != " " && pos2 != "[" && pos2 != "{" && pos2 != "}" && pos2 != "]" && pos2 != "#" ) {
							pressKey += pos2
						} else if ( pos2 == "]" || number == SONG.size() - 1 ) {
							this.press( pressKey, KEY_SPEED*speed )
							i = number
							break
						}
					}
				} else if ( pos == "#" ) {
					int j = i + 1
					for ( j; j < SONG.size(); j++ ) {
						String pos2 = SONG.substring( j, j + 1 )
						if ( pos2 == "#" ) {
							KEY_SPEED = SONG.substring( i+1, j ).toInteger()
							println KEY_SPEED
							i = j
							break
						}
					}
				} else {
					this.press( pos, KEY_SPEED*speed )
				}
			}
			times --
		}
	}

}